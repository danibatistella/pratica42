/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Main {
    public static void main(String[] args) {
       Pratica42 p42 = new Pratica42();
       System.out.println(p42.area);
       System.out.println(p42.perimetro);
       System.out.println(p42.Carea);
       System.out.println(p42.Cperimetro);
    }
}
